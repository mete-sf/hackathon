const extend = require("js-base/core/extend");
const Router = require("sf-core/ui/router");
const Color = require('sf-core/ui/color');
const StatusBarStyle = require('sf-core/ui/statusbarstyle');
const Data = require('sf-core/data');
const rau = require("sf-extension-utils").rau;

// Get generated UI code
var Page1Design = require("../ui/ui_page1");
var rauDetected = true;

const myOrange = Color.create("#B44408");

const Page1 = extend(Page1Design)(
    function(_super) {
        var self = this;
        _super(self);

        this.onShow = function() {
            self.headerBar.title = "Sudoku";
            self.headerBar.titleColor = Color.WHITE;
            self.headerBar.backgroundColor = myOrange;
            self.headerBar.itemColor = Color.WHITE;
            self.statusBar.android.color = myOrange;
            self.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;
            self.headerBar.leftItemEnabled = false;

            var bestTime = Data.getStringVariable('bestTime');

            if (bestTime == null) {
                Data.setStringVariable('bestTime', '99:99:99');
                self.bestTime.text = 'Your Best Time : --:--:--';
            }
            else {
                self.bestTime.text = 'Your Best Time : ' + bestTime;
            }
            
            try{
                checkRAU();
            }
            catch(e){
                
            }
            
        };

        this.playButton.onPress = playPress.bind(this);

    });

function checkRAU() {
    if (rauDetected) {
        rau.checkUpdate({
            showProgressCheck: true,
            showProgressErrorAlert: true,
            silent: false
        });
        rauDetected = false;
    }
}


function playPress() {
    Router.go("page2", {
        message: "Hello World!"
    });
}



module && (module.exports = Page1);
