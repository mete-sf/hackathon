const extend = require("js-base/core/extend");
const Router = require("sf-core/ui/router");
const Color = require('sf-core/ui/color');
const Label = require('sf-core/ui/label');
const TextAlignment = require('sf-core/ui/textalignment');
const Font = require('sf-core/ui/font');
const Timer = require("sf-core/timer");
const Sudoku = require("sudoku");
const AlertView = require('sf-core/ui/alertview');
const StatusBarStyle = require('sf-core/ui/statusbarstyle');
// Get generated UI code
var Page2Design = require("../ui/ui_page2");
var self;

var puzzle;
var solution;
var puzzleCopy;
var puzzleEditable;
var selectedBox;
var myTimeCounter;
var time;
var puzzleIsReady;
var myBoxArray;

const myGray = Color.create("#F2F2F2");
const myOrange = Color.create("#B44408");


const Page2 = extend(Page2Design)(
    function(_super) {
        self = this;
        _super(this);
        this._superOnShow = this.onShow;

        this.onHide = function() {
            Timer.clearTimer(myTimeCounter);
        };

        this.onShow = function() {

            createPuzzle();
            selectedBox;
            myTimeCounter;
            time = 0;
            puzzleIsReady = false;
            myBoxArray = [];

            self.headerBar.title = "Game";
            self.headerBar.titleColor = Color.WHITE;
            self.headerBar.backgroundColor = myOrange;
            self.headerBar.itemColor = Color.WHITE;
            self.statusBar.android.color = myOrange;
            self.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;

            Timer.setTimeout({
                task: gameAreaCreator,
                delay: 1000
            });

        };

        this.b0.touchEnabled = true;
        this.b1.touchEnabled = true;
        this.b2.touchEnabled = true;
        this.b3.touchEnabled = true;
        this.b4.touchEnabled = true;
        this.b5.touchEnabled = true;
        this.b6.touchEnabled = true;
        this.b7.touchEnabled = true;
        this.b8.touchEnabled = true;


        this.b0.onTouchEnded = function() {
            fillBox(0);
        };
        this.b1.onTouchEnded = function() {
            fillBox(1);
        };
        this.b2.onTouchEnded = function() {
            fillBox(2);
        };
        this.b3.onTouchEnded = function() {
            fillBox(3);
        };
        this.b4.onTouchEnded = function() {
            fillBox(4);
        };
        this.b5.onTouchEnded = function() {
            fillBox(5);
        };
        this.b6.onTouchEnded = function() {
            fillBox(6);
        };
        this.b7.onTouchEnded = function() {
            fillBox(7);
        };
        this.b8.onTouchEnded = function() {
            fillBox(8);
        };
        this.bSil.onTouchEnded = function() {
            fillBox(null);
        };
        this.solveButton.onTouch = function() {
            solvePuzzle();
        };
    });

function createPuzzle() {
    var puzzleTemp = Sudoku.makepuzzle();
    solution = Sudoku.solvepuzzle(puzzleTemp);
    puzzle = Sudoku.solvepuzzle(puzzleTemp);
    var i = 0;
    for (i = 0; i < 35; i++) {
        var rand = Math.floor((Math.random() * 80) + 0);
        puzzle[rand] = null;

    }
    puzzleCopy = puzzle;
    puzzleEditable = [];
}

function fillBox(data) {

    if (selectedBox != null) {
        if (puzzleEditable[selectedBox.id] == true) {

            puzzleCopy[selectedBox.id] = data;
            if (data == null) {
                selectedBox.text = "";
            }
            else {
                selectedBox.text = data;
            }
            if (JSON.stringify(puzzleCopy) === JSON.stringify(solution)) {
                completed();
            }
        }
    }

}

function solvePuzzle() {

    if (puzzleIsReady) {
        var i = 0;
        for (i = 0; i < myBoxArray.length; i++) {
            myBoxArray[i].text = solution[i];
        }
        completed();
    }

}

function completed() {

    Timer.clearTimer(myTimeCounter);

    var myAlertView = new AlertView({
        title: "Successful",
        message: "Puzzle is solved."
    });
    myAlertView.addButton({
        index: AlertView.ButtonType.POSITIVE,
        text: "OK",
        onClick: function() {
            Router.go('pgCompleted', {
                finishTime: self.timeText.text
            });
        }
    });

    myAlertView.show();
}

function gameAreaCreator() {

    var calculatedSize = self.gameArea.width / 10;
    var i = 0;
    var k = 0;
    var count = 0;

    for (i = 0; i < 9; i++) {
        for (k = 0; k < 9; k++) {

            // Label Create
            var myLabel = new Label({
                visible: true,
                borderWidth: 0.3,
                borderColor: Color.BLACK,
                id: count,
                font: Font.create("Arial", 20, Font.BOLD),
                textAlignment: TextAlignment.MIDCENTER,
                width: calculatedSize,
                height: calculatedSize
            });

            myBoxArray.push(myLabel);
            // puzzle null value check
            if (puzzle[count] == null) {
                myLabel.text = "";
                puzzleEditable[count] = true;
            }
            else {
                myLabel.text = puzzle[count];
                puzzleEditable[count] = false;
            }
            count++;

            // Label item set onTouch
            // Set border color and width
            myLabel.onTouch = function() {

                if (puzzleEditable[this.id] == true) {

                    if (selectedBox == null) {
                        this.borderWidth = 2;
                        this.borderColor = myOrange;
                        selectedBox = this;
                        self.layout.applyLayout();
                        return;
                    }

                    if (selectedBox.id != this.id) {
                        selectedBox.borderWidth = 0.3;
                        selectedBox.borderColor = Color.BLACK;

                        this.borderWidth = 2;
                        this.borderColor = myOrange;
                        selectedBox = this;
                        self.layout.applyLayout();
                        return;
                    }

                }

            };

            // Label set backgorund color for create gameArea 
            if (i < 3 || (i > 5 && i < 9)) {
                if (k == 3 || k == 4 || k == 5) {
                    myLabel.backgroundColor = myGray;
                }
                else {
                    myLabel.backgroundColor = Color.WHITE;
                }
            }
            else {
                if (k != 3 && k != 4 && k != 5) {
                    myLabel.backgroundColor = myGray;
                }
                else {
                    myLabel.backgroundColor = Color.WHITE;
                }
            }

            // Add label in gameArea layout
            self.gameArea.addChild(myLabel);

        }
    }

    // remove loading indicator on gameArea
    self.gameArea.removeChild(self.loading);
    self.layout.applyLayout();

    puzzleIsReady = true;
    // start time counter
    time = 0;
    myTimeCounter = Timer.setInterval({
        task: timeUpdater,
        delay: 1000
    });

}


function timeUpdater() {
    time++;

    var hours = Math.floor(time / 3600);
    var minutes = Math.floor((time % 3600) / 60);
    var seconds = Math.floor(time % 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }

    self.timeText.text = hours + ":" + minutes + ":" + seconds;
}


module && (module.exports = Page2);
