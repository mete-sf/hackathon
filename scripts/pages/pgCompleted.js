/* 
		You can modify its contents.
*/
const extend = require('js-base/core/extend');
const Color = require('sf-core/ui/color');
const StatusBarStyle = require('sf-core/ui/statusbarstyle');
const Router = require("sf-core/ui/router");
const Data = require('sf-core/data');
const Timer = require("sf-core/timer");

const PgCompletedDesign = require('ui/ui_pgCompleted');

var self;
var myTimeCounter;
var animCount;
var animFlag;

const myOrange = Color.create("#B44408");

const PgCompleted = extend(PgCompletedDesign)(
  // Constructor
  function(_super) {
    self = this;
    _super(this);

    this.button1.onTouchEnded = function() {
      Timer.clearTimer(myTimeCounter);
      Router.go("page1");
    };

    this.onShow = function(e) {

      self.headerBar.title = "Completed";
      self.headerBar.titleColor = Color.WHITE;
      self.headerBar.backgroundColor = myOrange;
      self.headerBar.itemColor = Color.WHITE;
      self.statusBar.android.color = myOrange;
      self.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT;
      self.headerBar.leftItemEnabled = false;

      var bestTime = Data.getStringVariable('bestTime');
      var a = bestTime.split(':');
      var secondsBest = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);

      var b = e.finishTime.split(':');
      var secondsFinish = (+b[0]) * 60 * 60 + (+b[1]) * 60 + (+b[2]);

      if (secondsFinish < secondsBest) {
        bestTime = e.finishTime;
        Data.setStringVariable('bestTime', e.finishTime);
      }
      self.finishTime.text = 'Finish Time : ' + e.finishTime;
      self.bestTime.text = 'Your Best Time : ' + bestTime;

      Timer.setTimeout({
        task: function() {
          animCount = 50;
          animFlag = true;
          myTimeCounter = Timer.setInterval({
            task: imageAnimator,
            delay: 10
          });
        },
        delay: 1000
      });

    };

  });


function imageAnimator() {
  if (animFlag) {
    animCount++;
    if (animCount > 250) {
      animFlag = false;
    }
  }
  else {
    animCount--;
    if (animCount < 50) {
      animFlag = true;
    }
  }
  self.myImage.top = animCount;
  self.myImage.applyLayout();
}


module && (module.exports = PgCompleted);
